import * as d3 from "d3";
const url = "https://d3-react-803d7.firebaseio.com/tallest-men.json";
const data = [32, 3, 23, 10, 8, 12, 18, 20];

export default class D3Chart {
  constructor(element) {
    const svg = d3
      .select(element)
      .append("svg")
      .attr("width", 800)
      .attr("height", 500);

    d3.json(url).then((heightsData) => {
      let rects = svg.selectAll("rect").data(heightsData);
      rects
        .enter()
        .append("rect")
        .attr("x", (d, i) => 50 * (i + 1))
        .attr("y", 50)
        .attr("width", 30)
        .attr("height", (d, i) => d.height * 2)
        .attr("fill", (d, i) => {
          return i % 2 == 0 ? "tomato" : "grey";
        });
    });
  }
}
