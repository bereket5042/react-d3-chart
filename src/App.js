import React from "react";
import logo from "./logo.svg";
import ChartWrapper from "./ChartWrapper.js";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
// import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">D3 Bar Chart</Navbar.Brand>
      </Navbar>
      <Container>
        <ChartWrapper />
      </Container>
    </div>
  );
}

export default App;
